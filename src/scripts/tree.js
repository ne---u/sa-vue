import * as PIXI from 'pixi.js';
import { viewportSize } from '@/scripts/utils';

class Tree {
  constructor(container) {
    this.DOMcontainer = container;
    this.width = this.DOMcontainer.offsetHeight;
    this.height = this.DOMcontainer.offsetHeight;

    this.app = new PIXI.Application({
      width: this.width,
      height: this.height,
      transparent: true,
    });
    this.renderer = null;

    this.stage = null;
    this.container2 = null;
    this.container = null;
    this.foreground = null;

    this.f = null;
    this.fg = null;
    this.fg2 = null;
    this.mousex = this.width / 2;
    this.mousey = this.height / 2;
    this.ploader = null;

    this.d = null;
    this.e = null;
    this.g = null;

    this.handlerMagic = () => { this.startMagic(); };
    this.handlerAnimate = () => { this.animate(); };
  }

  init() {
    this.renderer = this.app.renderer;
    this.DOMcontainer.appendChild(this.renderer.view);

    this.stage = new PIXI.Container();
    this.container2 = new PIXI.Container();
    this.container = new PIXI.Container();
    // this.foreground2 = new PIXI.Contaìiner();
    this.foreground = new PIXI.Container();

    this.stage.addChild(this.container);
    this.stage.addChild(this.foreground);
    this.stage.addChild(this.container2);

    this.ploader = new PIXI.loaders.Loader();

    this.loader();
  }

  loader() {
    this.ploader.add('fg', 'img/tree_red.png');
    this.ploader.add('depth', 'img/tree_red_depth.jpg');

    this.ploader.add('fg2', 'img/tree_white.png');
    this.ploader.add('depth2', 'img/tree_white_depth.jpg');

    this.ploader.once('complete', this.handlerMagic);
    this.ploader.load();
  }

  startMagic() {
    this.fg = new PIXI.Sprite(this.ploader.resources.fg.texture);
    this.fg2 = new PIXI.Sprite(this.ploader.resources.fg2.texture);

    this.foreground.addChild(this.fg);
    this.foreground.addChild(this.fg2);

    this.d = new PIXI.Sprite(this.ploader.resources.depth.texture);
    this.f = new PIXI.filters.DisplacementFilter(this.d, 0);
    this.fg.filters = [this.f];

    window.onmousemove = (e) => {
      this.mousex = e.clientX;
      this.mousey = e.clientY;
    };
    this.e = new PIXI.Sprite(this.ploader.resources.depth2.texture);
    this.g = new PIXI.filters.DisplacementFilter(this.e, 0);
    this.fg2.filters = [this.g];

    window.onmousemove = (e) => {
      this.mousex = e.clientX;
      this.mousey = e.clientY;
    };

    this.handlerAnimate();
  }

  animate() {
    this.f.scale.x = ((window.innerWidth / 2) - this.mousex) / 50;
    this.f.scale.y = ((window.innerHeight / 2) - this.mousey) / 50;
    this.g.scale.x = ((window.innerWidth / 2) - this.mousex) / 70;
    this.g.scale.y = ((window.innerHeight / 2) - this.mousey) / 70;
    this.fg.addChild(this.d);
    this.fg2.addChild(this.e);
    this.d.renderable = false;
    this.e.renderable = false;

    this.renderer.render(this.stage);
    requestAnimationFrame(this.handlerAnimate);
  }
}

export default Tree;
