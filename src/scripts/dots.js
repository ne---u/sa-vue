import * as PIXI from 'pixi.js';
import { viewportSize, getRandomInt } from '@/scripts/utils';

class Dots {
  constructor(container) {
    this.app = new PIXI.Application({
      width: viewportSize().x,
      height: viewportSize().y,
      transparent: true,
      forceFXAA: true,
    });

    this.container = container;
    this.dots = [];
    this.totalDots = viewportSize().x < 769 ? 200 : 500;

    this.dotSprite = null;
    this.tick = 0;

    this.dotsBoundsPadding = 100;

    this.dotsBounds = null;

    this.colors = [0xe97676, 0xb0b0b0, 0x4f4f4f, 0x333333, 0x8c8c8c, 0x737373]
  }

  init() {
    this.container.appendChild(this.app.view);

    for (let i = 0; i < this.totalDots; i += 1) {
      const dotGraphics = new PIXI.Graphics();
      dotGraphics.beginFill(this.colors[Math.floor(Math.random() * this.colors.length)]);
      dotGraphics.drawCircle(0, 0, getRandomInt(2, 5));
      dotGraphics.endFill();

      const texture = new PIXI.Texture(dotGraphics.generateCanvasTexture());

      this.dotSprite = new PIXI.Sprite(texture);

      this.dotSprite.x = Math.floor(Math.random() * this.app.screen.width);
      this.dotSprite.y = Math.floor(Math.random() * this.app.screen.height);

      this.dotSprite.blendMode = Math.random() > 0.5 ? PIXI.BLEND_MODES.OVERLAY : PIXI.BLEND_MODES.MULTIPLY;

      this.dotSprite.direction = Math.random() * Math.PI * 2;

      this.dotSprite.turningSpeed = Math.random() - 0.8;

      this.dotSprite.speed = (1 + Math.random()) * 1.2;

      this.dots.push(this.dotSprite);

      this.app.stage.addChild(this.dotSprite);
    }

    this.dotsBounds = new PIXI.Rectangle(
      -this.dotsBoundsPadding,
      -this.dotsBoundsPadding,
      (this.app.screen.width + this.dotsBoundsPadding) * 2,
      (this.app.screen.height + this.dotsBoundsPadding) * 2,
    );


    this.app.ticker.add(() => {
      this.fnTicker();
    });
  }

  fnTicker() {
    for (let i = 0; i < this.dots.length; i += 1) {
      this.dotSprite = this.dots[i];

      this.dotSprite.direction += this.dotSprite.turningSpeed * 0.01;
      this.dotSprite.x += Math.sin(this.dotSprite.direction) * this.dotSprite.speed;
      this.dotSprite.y += Math.cos(this.dotSprite.direction) * this.dotSprite.speed;
      this.dotSprite.rotation = -this.dotSprite.direction - (Math.PI / 2);


      if (this.dotSprite.x < this.dotsBounds.x) {
        this.dotSprite.x += this.dotsBounds.width;
      } else if (this.dotSprite.x > this.dotsBounds.x + this.dotsBounds.width) {
        this.dotSprite.x -= this.dotsBounds.width;
      }

      if (this.dotSprite.y < this.dotsBounds.y) {
        this.dotSprite.y += this.dotsBounds.height;
      } else if (this.dotSprite.y > this.dotsBounds.y + this.dotsBounds.height) {
        this.dotSprite.y -= this.dotsBounds.height;
      }
    }
    this.tick += 0.1;
  }
}

export default Dots;
