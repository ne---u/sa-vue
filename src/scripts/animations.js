import gsap from 'gsap';
import SplitText from '@/scripts/SplitText';

const animations = new Map();

animations.set('bg', (color, el) => {
  gsap.to(el, 2.5, {
    backgroundColor: color,
    ease: gsap.Expo.easeOut,
  });
});

animations.set('text', (el) => {
  gsap.fromTo(el, 1.5, {
    yPercent: 10,
    skewY: 5,
    autoAlpha: 0,
    transformOrigin: '0 0',
  }, {
    delay: 0.2,
    yPercent: 0,
    skewY: 0,
    autoAlpha: 1,
    ease: gsap.Expo.easeOut,
    clearProps: 'all',
  });
});

animations.set('picture', (el) => {
  gsap.fromTo(el, 1.5, {
    yPercent: 10,
    skewY: 5,
    autoAlpha: 0,
    transformOrigin: '0 0',
  }, {
    yPercent: 0,
    skewY: 0,
    autoAlpha: 1,
    ease: gsap.Expo.easeOut,
    clearProps: 'all',
  });
});

export default animations;

// const lerp = (a, b, n) => ((1 - n) * a) + (n * b);
